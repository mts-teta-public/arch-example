package application

import (
	"auth/internal/adpters/http"
	"auth/internal/adpters/memory"
	"auth/internal/domain/usecases"
	"auth/pkg/infra/logger"
	"auth/pkg/infra/metrics"
	"auth/pkg/infra/probes"
	"context"
)

type App struct{
	l logger.Logger
	shutdownFuncs []func(ctx context.Context) error
}

func New(l logger.Logger) *App{
	return &App{
		l:l,
	}
}

func (app *App) Start() error {
	probes, _ := probes.New(app.l)
	probes.SetStarted()
	err := probes.Start()
	if err != nil {
		app.l.Sugar().Fatalf("probes start failed: %s", err.Error())
	}

	me, err := metrics.New()
	if err != nil {
		app.l.Sugar().Fatalf("Metrics init failed: %s", err.Error())
	}
	app.shutdownFuncs = append(app.shutdownFuncs, me.Stop)
	
	userStorage, err := memory.New()
	if err != nil {
		app.l.Sugar().Fatalf("create user storage failed: %s", err.Error())
	}

	auth, err := usecases.New(userStorage)
	if err != nil {
		app.l.Sugar().Fatalf("create buissness logic failed: %s", err.Error())
	}

	s, err := http.New(auth, app.l)
	if err != nil {
		app.l.Sugar().Fatalf("server not started %s", err.Error())
	}
	app.shutdownFuncs = append(app.shutdownFuncs, s.Stop)
	err = s.Start()
	if err != nil {
		app.l.Sugar().Fatalf("server not started: %s", err.Error())
	}

	probes.SetReady()

	return nil
} 

func (a *App) Stop(ctx context.Context) error {
	var err error
	for i := len(a.shutdownFuncs)-1; i>=0; i-- {
		err  = a.shutdownFuncs[i](ctx)
		if  err != nil {
			a.l.Sugar().Error(err)
		}
	}

	a.l.Info("app stopped")

	return nil
} 