package usecases

import (
	"auth/internal/domain/models"
	"auth/internal/ports"
	"auth/pkg/infra/metrics"
	"bytes"
	"context"
	"crypto/sha1"
	"errors"
	"fmt"
	"time"

	"github.com/caarlos0/env"
	"github.com/golang-jwt/jwt/v4"
	"github.com/xdg-go/pbkdf2"
	"go.opentelemetry.io/otel/trace"
)

type Auth struct {
	cfg *Config

	userStorage ports.UserStorage
}

type Config struct {
	Salt string `env:"SALT" envDefault:"my_salt"`
	Secret string `env:"TOKEN_SECRET" envDefault:"test"`

	AccessTokenExpired  time.Duration `env:"ACCESS_TOKEN_EXPIRED_IN" envDefault:"5m"`
	RefreshTokenExpired time.Duration `env:"REFRESH_TOKEN_EXPIRED_IN" envDefault:"1h"`
}

func (c *Config) Parse() error {
	if err := env.Parse(c); err != nil {
		return err
	}

	return nil
}



func New(userStorage ports.UserStorage) (*Auth, error) {
	cfg := new(Config)
	err := cfg.Parse()
	if err != nil {
		return nil, fmt.Errorf("parse domain config failed: %w", err)
	}

	return &Auth{
		userStorage: userStorage,
	}, nil
}

func (a *Auth) Login(ctx context.Context, login string, password string) (string, string, error) {
	ctx, span := metrics.FollowSpan(ctx)
	defer span.End()

	user, err := a.userStorage.GetUser(ctx, login)
	if err != nil {
		return "", "", err
	}
	cryptPass := a.EncodePassword(password)
	if !bytes.Equal(cryptPass, []byte(user.Password)) {
		return "", "", models.ErrForbidden
	}

	access, err := a.generateToken(login, a.cfg.AccessTokenExpired)
	if err != nil {
		return "", "", err
	}

	refresh, err := a.generateToken(login, a.cfg.RefreshTokenExpired)
	if err != nil {
		return "", "", err
	}

	return access, refresh, nil
}

func (a *Auth) Verify(ctx context.Context, access string, refresh string) (r models.VerifyResponse, err error) {
	var span trace.Span
	ctx, span = metrics.FollowSpan(ctx)
	defer span.End()

	var login string
	login, err = a.verifyToken(access)
	if err != nil && !errors.Is(err, models.ErrTokenExpired) {
		metrics.SetError(span, err)
		return r, err
	}

	if err != nil {
		login, err = a.verifyToken(refresh)
		if err != nil {
			metrics.SetError(span, err)
			return r, err
		}
	}

	r.User, err = a.userStorage.GetUser(ctx, login)
	if err != nil {
		metrics.SetError(span, err)
		return r, err
	}

	r.AccessToken, err = a.generateToken(login, a.cfg.AccessTokenExpired)
	if err != nil {
		metrics.SetError(span, err)
		return r, err
	}

	r.RefreshToken, err = a.generateToken(login, a.cfg.RefreshTokenExpired)
	if err != nil {
		metrics.SetError(span, err)
		return r, err
	}

	return r, nil
}

func (a *Auth) EncodePassword(password string) []byte {
	dk := pbkdf2.Key([]byte(password), []byte(a.cfg.Salt), 4096, 32, sha1.New)
	return dk
}

func (a *Auth) generateToken(login string, expiredIn time.Duration) (string, error) {
	mySigningKey := a.cfg.Secret

	now := time.Now()
	// Create the Claims
	claims := &jwt.RegisteredClaims{
		ExpiresAt: jwt.NewNumericDate(now.Add(expiredIn)),
		Issuer:    login,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", fmt.Errorf("generate token failed: %w", err)
	}
	return ss, nil
}

func (a *Auth) verifyToken(tokenString string) (string, error) {
	var claims jwt.RegisteredClaims
	token, err := jwt.ParseWithClaims(tokenString, &claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(a.cfg.Secret), nil
	})
	if !token.Valid {
		return "", fmt.Errorf("parse token unexpected error: %w", err)
	} else if errors.Is(err, jwt.ErrTokenMalformed) {
		return "", models.ErrTokenInvalid
	} else if errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet) {
		// Token is either expired or not active yet
		return "", models.ErrTokenExpired
	}

	return claims.Issuer, nil
}
