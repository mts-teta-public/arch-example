package models

type User struct {
	Login string
	Password string
	Email string
}

type VerifyResponse struct {
	AccessToken  string
	RefreshToken string
	User         User
}