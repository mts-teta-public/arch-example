package memory

import (
	"auth/internal/domain/models"
)

type MemoryStorage struct {
	storage map[string]models.User
}

func New() (*MemoryStorage, error) {
	storage := make(map[string]models.User, 3)
	storage["test"] = models.User{}
	storage["test2"] = models.User{}

	return &MemoryStorage{
		storage: storage,
	}, nil
}
