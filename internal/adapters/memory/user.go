package memory

import (
	"auth/internal/domain/models"
	"auth/pkg/infra/metrics"
	"context"
	"fmt"

	"github.com/juju/zaputil/zapctx"
	"go.uber.org/zap"
)

func (s *MemoryStorage) GetUser(ctx context.Context, login string) (models.User, error) {
	ctx, span := metrics.FollowSpan(ctx)
	defer span.End()

	u, ok := s.storage[login]
	if !ok {
		zapctx.Error(ctx, "user not found", zap.String("user", login))
		err := fmt.Errorf("%w: user", models.ErrNotFound)
		metrics.SetError(span, err)
		return models.User{}, err
	}

	return u, nil
}
