package http

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/juju/zaputil/zapctx"
	"go.uber.org/zap"
)

const (
	accessHeader  = "access_token"
	refreshHeader = "refresh_header"
)

type UserProjection struct {
	Login string `json:"login"`
	Email string `json:"email"`
}

// Login
// @ID Login
// @tags auth
// @Summary Generate auth tokens.
// @Description Validate credentials, return access and refresh tokens.
// @Success 200
// @Failure 403 {string} string "forbidden"
// @Failure 500 {string} string "internal error"
// @Router /login [post]
func (a *Adapter) login(ctx *gin.Context) {
	login, password, ok := ctx.Request.BasicAuth()
	if !ok {
		ctx.Header("WWW-Authenticate", "Basic")
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"error": "not auth data",
		})
		return
	}

	ctx.Request = ctx.Request.WithContext(zapctx.WithFields(ctx.Request.Context(),
		zap.String("login", login),
	))

	accesstoken, refreshToken, err := a.auth.Login(ctx, login, password)
	if err != nil {
		a.BindError(ctx, err)
		return
	}

	ctx.SetCookie(accessHeader, accesstoken, 0, "", "auth", true, true)
	ctx.SetCookie(refreshHeader, refreshToken, 0, "", "auth", true, true)
}

// Validate
// @ID Validate
// @tags auth
// @Summary Validate tokens
// @Description Validate tokens and refresh tokens if refresh token is valid
// @Security Auth
// @Body {object} http.UserProjection true "example"
// @Failure 400 {object} gin.H "bad request"
// @Failure 403 {string} string "forbidden"
// @Failure 500 {string} string "internal error"
// @Router /validate [post]
func (a *Adapter) verify(ctx *gin.Context) {
	access, err := ctx.Cookie(accessHeader)
	if err != nil {
		return
	}
	refresh, err := ctx.Cookie(refreshHeader)
	if err != nil {
		return
	}

	user, err := a.auth.Verify(ctx, access, refresh)
	if err != nil {
		a.BindError(ctx, err)
		return
	}

	ctx.SetCookie(accessHeader, user.AccessToken, 0, "", "auth", true, true)
	ctx.SetCookie(refreshHeader, user.RefreshToken, 0, "", "auth", true, true)
	ctx.JSON(http.StatusOK, UserProjection{
		Login: user.User.Login,
		Email: user.User.Email,
	})
}
