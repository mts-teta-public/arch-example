//go:build containers
// +build containers

package tests

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

func TestRunSuite(t *testing.T) {
	suite.Run(t, new(ExampleSuit))
}

type ExampleSuit struct {
	suite.Suite

	server *http.Server
}

func (s *ExampleSuit) SetupSuite() {
	server := http.Server{}
	go func() {
		server.Start()
	}()
	s.server = &server

	s.T().Log("Suite setup is done")
}

func (s *ExampleSuit) TearDownSuite() {
	s.T().Log("Suite stop is done")
}
