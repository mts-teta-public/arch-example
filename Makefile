.PHONY: lint
lint:
	golangci-lint run -c .golangci.yml

.PHONY: formate
formate:
	gofumpt -l -w .

.PHONY: swagger
swagger:
	docker build --tag swaggo/swag:latest . --file swaggo.Dockerfile && \
		docker run --rm --volume ${PWD}:/app --workdir /app swaggo/swag:latest /root/swag init \
		--parseDependency \
		--dir ./internal/adapters/http \
		--generalInfo swagger.go \
		--output ./api/swagger/public \
		--parseDepth 1

.PHONY: test/local
tests/local: 
	go test -tags=tests ./internal/adapters/http

.PHONY: test/integration
tests/integration: 
	go test -tags=testing ./internal/tests

.PHONY: test/integration/testify
tests/integration/testify: 
	go test -tags=testify ./internal/tests

.PHONY: test/integration/containers
tests/integration/containers: 
	go test -tags=containers ./internal/tests

.PHONY: test/integration/bdd
tests/integration/bdd: 
	go test -tags=bdd ./internal/tests	

.PHONY: test/integration/verbose
tests/integration/verbose: 
	go test -v ./internal/tests
